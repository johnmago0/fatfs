.PHONY: directory clean debug image

IO_SRC := src/io/volume.c

UTILS_SRC := src/utils/pathparser.c src/utils/timestamp.c

FILESYSTEM_SRC := src/filesystem/bpb.c src/filesystem/fat.c 
FILESYSTEM_SRC += src/filesystem/directory.c src/filesystem/fs_api.c 

SRC := $(IO_SRC) $(UTILS_SRC) $(FILESYSTEM_SRC) src/main.c 
OBJS := $(patsubst src/%.c, build/%.o, $(SRC))

CC := clang
CFLAGS ?= -g -Wall -Wshadow -Wconversion -Wextra -Wunreachable-code -Wunused -Wcast-align
LDFLAGS ?= -fuse-ld=mold
INCLUDES := -Iinclude/

all: $(OBJS)
	$(CC) $(LDFLAGS) $(INCLUDES) $(OBJS) -o build/fatfs

directory:
	@mkdir -p build
	@mkdir -p build/utils
	@mkdir -p build/io
	@mkdir -p build/filesystem

build/%.o: src/%.c directory
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $@

image:
	dd if=/dev/zero of=build/test.img count=5 bs=1M
	mkfs.fat -F 12 build/test.img

fat-image:
	dd if=/dev/zero of=build/fat-test.img count=5 bs=1M
	mkfs.fat -F 12 build/fat-test.img

fat-test: directory fat-image
	$(CC) $(CFLAGS) $(INCLUDES) src/filesystem/bpb.c src/filesystem/fat.c \
	src/io/volume.c test/fat/test_fast.c -o build/fat-test 

directory-image:
	dd if=/dev/zero of=build/directory-test.img count=5 bs=1M
	mkfs.fat -F 12 build/directory-test.img
	mcopy -i build/directory-test.img test/directory/input.txt  ::/

directory-test: directory directory-image
	$(CC) $(CFLAGS) $(INCLUDES) \
	src/filesystem/bpb.c src/filesystem/directory.c src/filesystem/fat.c src/filesystem/fs_api.c \
	src/io/volume.c src/utils/pathparser.c src/utils/timestamp.c \
	test/directory/test_directory.c -o build/directory-test 

time-test: directory
	$(CC) $(CFLAGS) $(INCLUDES) src/utils/timestamp.c test/time/test_time.c -o build/time-test 

parser-test: directory
	$(CC) $(CFLAGS) $(INCLUDES) src/utils/pathparser.c test/parser/test_parser.c -o build/parser-test 

# valgrind-command:
# valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose <program> <args>

clean:
	@rm -rf build/