#include "filesystem/bpb.h"
#include "filesystem/fs_api.h"

#include <stdio.h>
#include <string.h>

#define INPUT_TEXT "input.txt"
#define TEST_TEXT "test/directory/input.txt"

#define FILE_SIZE 29254

#define TEST_FUNC(func)                         \
  func() == 0 ? printf("'%s': PASSED\n", #func) \
              : printf("'%s': FAILED\n", #func)

/******************************************************************************
 * UTILS
 ******************************************************************************/

int file_compare(void *buffer, size_t size, long offset, int seek)
{
  if (buffer == NULL) {
    return -1;
  }

  void *source = calloc(1, size);
  if (source == NULL) {
    printf("ERROR: %s could not allocate a buffer for the text\n", TEST_TEXT);
    return -1;
  }

  FILE *fp = fopen(TEST_TEXT, "r");
  if (fp == NULL) {
    printf("ERROR: %s could not be opened\n", TEST_TEXT);
    return -1;
  }

  if (offset != 0 && fseek(fp, offset, seek) != 0) {
    printf("ERROR: %s could not move pointer\n", TEST_TEXT);
    free(source);
    (void)fclose(fp);
    return -1;
  }

  if (fread(source, size, 1, fp) != 1) {
    printf("ERROR: %s could not be readed\n", TEST_TEXT);
    free(source);
    (void)fclose(fp);
    return -1;
  }

  if (memcmp(buffer, source, size) != 0) {
    (void)fclose(fp);
    free(source);
    return -1;
  }

  free(source);
  (void)fclose(fp);
  return 0;
}

/******************************************************************************
 * TESTS
 ******************************************************************************/

int testing_file_read_all()
{
  file_t *file = fs_open(INPUT_TEXT);
  if (file == NULL) {
    return -1;
  }

  void *buffer = malloc(sizeof(char) * FILE_SIZE);
  if (buffer == NULL) {
    return -1;
  }

  if (fs_read(file, buffer, sizeof(char) * FILE_SIZE)
      != sizeof(char) * FILE_SIZE) {
    fs_close(file);
    free(buffer);
    return -1;
  }

  if (file_compare(buffer, sizeof(char) * FILE_SIZE, 0, SEEK_SET) < 0) {
    fs_close(file);
    free(buffer);
    return -1;
  }

  fs_close(file);
  free(buffer);
  return 0;
}

int testing_file_read_byte_offset()
{
  long offset = 10;

  file_t *file = fs_open(INPUT_TEXT);
  if (file == NULL) {
    return -1;
  }

  void *buffer = malloc(sizeof(char) * (size_t)offset);
  if (buffer == NULL) {
    fs_close(file);
    return -1;
  }

  file->byte += offset;
  if (fs_read(file, buffer, sizeof(char) * (size_t)offset)
      != (ssize_t)(sizeof(char) * (size_t)offset)) {
    fs_close(file);
    free(buffer);
    return -1;
  }

  if (file_compare(buffer, sizeof(char) * (size_t)offset, offset, SEEK_SET)
      < 0) {
    fs_close(file);
    free(buffer);
    return -1;
  }

  fs_close(file);
  free(buffer);
  return 0;
}

int testing_file_read_next_bytes()
{
  long offset = 10;

  file_t *file = fs_open(INPUT_TEXT);
  if (file == NULL) {
    return -1;
  }

  void *buffer = malloc(sizeof(char) * (size_t)offset);
  if (buffer == NULL) {
    fs_close(file);
    return -1;
  }

  if (fs_read(file, buffer, sizeof(char) * (size_t)offset)
      != (ssize_t)(sizeof(char) * (size_t)offset)) {
    fs_close(file);
    free(buffer);
    return -1;
  }

  if (file_compare(buffer, sizeof(char) * (size_t)offset, 0, SEEK_SET) < 0) {
    fs_close(file);
    free(buffer);
    return -1;
  }

  fs_close(file);
  free(buffer);
  return 0;
}

int testing_file_seek_set()
{
  file_t *file = fs_open(INPUT_TEXT);
  if (file == NULL) {
    return -1;
  }

  DWORD tinyOffset = 256;
  DWORD mediumOffset = 2176;
  DWORD bigOffset = file->stat->fileSize + 10;
  long negOffset = -1;

  if (fs_seek(file, tinyOffset, SET) < 0) {
    return -1;
  }

  if (!(file->byte == tinyOffset && file->cluster == file->stat->cluster)) {
    return -1;
  }

  if (fs_seek(file, mediumOffset, SET) < 0) {
    return -1;
  }

  if (!(file->byte == mediumOffset - bpb_cluster_size()
        && file->cluster != file->stat->cluster)) {
    return -1;
  }

  if (fs_seek(file, bigOffset, SET) < 0) {
    return -1;
  }

  if (!(file->byte == 0 && file->cluster != file->stat->cluster)) {
    return -1;
  }


  if (fs_seek(file, negOffset, SET) < 0) {
    return -1;
  }

  if (file->byte != 0 && file->cluster != file->stat->cluster) {
    return -1;
  }

  return 0;
}

int main(int argc, char **argv)
{
  if (argc < 2) {
    printf("Invalid number of arguments\n");
    return -1;
  }

  if (fs_mount(argv[1]) < 0) {
    printf("Unable to mount the volume\n");
    return -1;
  }

  TEST_FUNC(testing_file_read_all);
  TEST_FUNC(testing_file_read_byte_offset);
  TEST_FUNC(testing_file_read_next_bytes);
  TEST_FUNC(testing_file_seek_set);

  fs_umount();
  return 0;
}
