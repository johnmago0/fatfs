#include "filesystem/type.h"
#include "utils/timestamp.h"

#include <stdio.h>
#include <time.h>

#define TEST_FUNC(func)                         \
  func() == 0 ? printf("'%s': PASSED\n", #func) \
              : printf("'%s': FAILED\n", #func)

#define to_sec(sec) ((sec) - ((sec) % 2 ? 1 : 0))

int testing_timestamp_current_time()
{
  time_t curTime = time(NULL);
  struct tm *curLocalTime = localtime(&curTime);

  WORD timestamp = timestamp_current_time();

  if (curLocalTime->tm_hour != HOURS(timestamp))
    return -1;

  if (curLocalTime->tm_min != MINUTES(timestamp))
    return -1;

  if (to_sec(curLocalTime->tm_sec) != SECONDS(timestamp))
    return -1;

  return 0;
}

int testing_timestamp_current_date()
{
  time_t curDate = time(NULL);
  struct tm *curLocalDate = localtime(&curDate);

  WORD timestamp = timestamp_current_date();

  if (curLocalDate->tm_mday != DAY(timestamp)) {
    printf("cur: %02d\n", curLocalDate->tm_mday);
    printf("DAY: %02d\n", DAY(timestamp));
  }

  if (curLocalDate->tm_mon != MONTH(timestamp)) {
    printf("cur: %02d\n", curLocalDate->tm_mon);
    printf("MONTH: %02d\n", MONTH(timestamp));
  }

  if (curLocalDate->tm_year + 1900 != YEAR(timestamp)) {
    printf("cur: %02d\n", curLocalDate->tm_year);
    printf("YEAR: %02d\n", YEAR(timestamp));
  }

  return 0;
}

int main()
{
  TEST_FUNC(testing_timestamp_current_time);
  TEST_FUNC(testing_timestamp_current_date);
  return 0;
}
