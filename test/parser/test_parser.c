#include "utils/pathparser.h"

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEST_FUNC(func)                         \
  func() == 0 ? printf("'%s': PASSED\n", #func) \
              : printf("'%s': FAILED\n", #func)

int testing_parser_create_empty()
{
  PathParser *parser = parser_create("");
  int res = 0;
  if (parser != NULL) {
    res = -1;
  }

  parser_delete(&parser);
  return res;
}

int testing_parser_create_root()
{
  PathParser *parser = parser_create("/");
  int res = 0;
  if (parser != NULL) {
    res = -1;
  }

  parser_delete(&parser);
  return res;
}

int testing_parser_create_null()
{
  PathParser *parser = parser_create(NULL);
  int res = 0;
  if (parser != NULL) {
    res = -1;
  }

  parser_delete(&parser);
  return res;
}

int testing_parser_create_max_path()
{
  char path[PATH_MAX];
  for (size_t i = 0; i < PATH_MAX; i++) {
    path[i] = 'l';
  }

  PathParser *parser = parser_create(path);
  int res = 0;
  if (parser != NULL) {
    res = -1;
  }

  parser_delete(&parser);
  return res;
}

int testing_parser_create_double_slash()
{
  char path[] = "this/have//double/slash";

  PathParser *parser = parser_create(path);
  int res = 0;
  if (parser != NULL) {
    res = -1;
  }

  parser_delete(&parser);
  return res;
}

int testing_parser_create_invalid_symbol()
{
  char path[] = "this/is/a/invalid/*/symbol";

  PathParser *parser = parser_create(path);
  int res = 0;
  if (parser != NULL) {
    res = -1;
  }

  parser_delete(&parser);
  return res;
}

int testing_parser_tokenization()
{
  char path[] = "this/is/an/./example/../valid/path";

  PathParser *parser = parser_create(path);
  int tokenRes = 0;

  if (strncmp(parser->curToken, "THIS       ", 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, "IS         ", 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, "AN         ", 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, DOT_FILENAME, 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, "EXAMPLE    ", 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, DOTDOT_FILENAME, 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, "VALID      ", 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 0) {
    return -1;
  }

  if (strncmp(parser->curToken, "PATH       ", 11) != 0) {
    parser_delete(&parser);
    return -1;
  }

  tokenRes = parser_token(parser);
  if (tokenRes != 1) {
    return -1;
  }

  parser_delete(&parser);
  return 0;
}

int main()
{
  TEST_FUNC(testing_parser_create_empty);
  TEST_FUNC(testing_parser_create_root);
  TEST_FUNC(testing_parser_create_null);
  TEST_FUNC(testing_parser_create_max_path);
  TEST_FUNC(testing_parser_create_double_slash);
  TEST_FUNC(testing_parser_create_invalid_symbol);
  TEST_FUNC(testing_parser_tokenization);
  return 0;
}
