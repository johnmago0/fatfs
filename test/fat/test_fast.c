#include "filesystem/bpb.h"
#include "filesystem/fat.h"
#include "filesystem/type.h"
#include "io/volume.h"

#include <stdio.h>
#include <string.h>

#define TEST_FUNC(func)                         \
  func() == 0 ? printf("'%s': PASSED\n", #func) \
              : printf("'%s': FAILED\n", #func)


// When creating the FAT via mkfat, the first available cluster is this.
#define START_FAT 3

/******************************************************************************
 * UTILS
 ******************************************************************************/

void init_filesystem(const char *pathname)
{
  volume_open(pathname);
  bpb_open();
}

void close_filesystem()
{
  volume_close();
  bpb_close();
}

int fat_comp()
{
  void *fat1 = calloc(bpb_fat_size(), bpb_bytes_per_sec());
  void *fat2 = calloc(bpb_fat_size(), bpb_bytes_per_sec());

  if (fat1 == NULL || fat2 == NULL) {
    return -1;
  }

  DWORD offset = bpb_fat_sec_num(bpb_fat_offset(0)) * bpb_bytes_per_sec();

  if (volume_read(offset + 0 * bpb_fat_size(), fat1, bpb_bytes_per_sec()) < 0) {
    return -1;
  }

  if (volume_read(offset + 1 * bpb_fat_size(), fat2, bpb_bytes_per_sec()) < 0) {
    return -1;
  }

  int res = memcmp(fat1, fat2, (DWORD)bpb_fat_size() * bpb_bytes_per_sec());

  free(fat1);
  free(fat2);

  return res == 0 ? 0 : -1;
}

/******************************************************************************
 * TESTS
 ******************************************************************************/

int testing_fat_full_write()
{
  WORD value = 0x00;
  for (DWORD i = START_FAT; i < bpb_count_of_clusters() - START_FAT; i++) {
    if (fat_write(i, value)) {
      return -1;
    }

    value = (value + 1) % 0xFF;
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int testing_fat_random_read()
{
  srand(1);

  for (DWORD i = 0; i < 100; i++) {
    WORD val1 = 0;
    WORD val2 = 0;
    DWORD cluster = (DWORD)rand() % bpb_count_of_clusters();

    if (fat_read(cluster, 0, &val1) < 0) {
      printf("Aqui1: %d\n", i);
      return -1;
    }

    if (fat_read(cluster, 1, &val2) < 0) {
      printf("Aqui2: %d\n", i);
      return -1;
    }

    if (val1 != val2) {
      printf("Aqui3: %d\n", i);
      return -1;
    }
  }

  return 0;
}

int testing_fat_zeros_write()
{
  WORD value = 0x00;
  for (DWORD i = 0; i < bpb_count_of_clusters(); i++) {
    if (fat_write(i, value)) {
      return -1;
    }
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int testing_fat_random_write()
{
  srand(1);

  for (DWORD i = 0; i < 200; i++) {
    if (fat_write((DWORD)rand() % bpb_count_of_clusters(), (WORD)bpb_fat_eof())
        < 0) {
      return -1;
    }
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int testing_fat_allocate_chain()
{
  DWORD cluster = 0;
  if (fat_allocate_chain((DWORD)10, &cluster) < 0) {
    return -1;
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int testing_fat_allocate_cluster()
{
  DWORD cluster = 0;
  if (fat_allocate_cluster(0, &cluster) < 0) {
    return -1;
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int testing_fat_deallocate_cluster()
{
  if (fat_deallocate_cluster(0) < 0) {
    return -1;
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int testing_fat_deallocate_chain()
{
  if (fat_deallocate_chain(0) < 0) {
    return -1;
  }

  if (fat_comp() != 0) {
    return -1;
  }

  return 0;
}

int main(int argc, char **argv)
{
  init_filesystem(argv[1]);

  TEST_FUNC(testing_fat_full_write);
  TEST_FUNC(testing_fat_random_read);
  TEST_FUNC(testing_fat_zeros_write);
  TEST_FUNC(testing_fat_allocate_chain);
  TEST_FUNC(testing_fat_allocate_cluster);
  TEST_FUNC(testing_fat_deallocate_cluster);
  TEST_FUNC(testing_fat_deallocate_chain);
  TEST_FUNC(testing_fat_random_write);

  close_filesystem();
}
