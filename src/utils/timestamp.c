#include "utils/timestamp.h"

#include <sys/time.h>
#include <time.h>

#define to_sec(sec) ((sec) - ((sec) % 2 ? 1 : 0))

WORD timestamp_current_time()
{
  time_t curTime = time(NULL);
  struct tm *curLocalTime = localtime(&curTime);

  int sec = to_sec(curLocalTime->tm_sec);
  return (WORD)TIME_STAMP(
    (BYTE)curLocalTime->tm_hour, (BYTE)curLocalTime->tm_min, (BYTE)sec);
}

WORD timestamp_current_date()
{
  time_t curDate = time(NULL);
  struct tm *curLocalDate = localtime(&curDate);

  int year = curLocalDate->tm_year + 1900;

  return (WORD)DATE_STAMP(
    (BYTE)curLocalDate->tm_mday, (BYTE)curLocalDate->tm_mon, (BYTE)year);
}

BYTE timestamp_current_milliseconds()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);

  return (BYTE)((long)tv.tv_usec / (long)10000);
}
