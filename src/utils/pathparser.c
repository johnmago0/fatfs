#include "utils/pathparser.h"

#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#define NAME_SIZE 8
#define EXTENSION_SIZE 3
#define FILENAME_SIZE (NAME_SIZE + EXTENSION_SIZE)

#define isspecial(c)                                                      \
  ((c) == '$' || (c) == '%' || (c) == '/' || (c) == '-' || (c) == '_'     \
    || (c) == '@' || (c) == '~' || (c) == '`' || (c) == '!' || (c) == '(' \
    || (c) == ')' || (c) == '{' || (c) == '}' || (c) == '^' || (c) == '#' \
    || (c) == '&' || (c) == '.')

/******************************************************************************
 *                              PUBLIC FUNCTIONS
 ******************************************************************************/

/**
 * @brief Validates the dot position in the filepath
 *
 * In the FAT specification the dot can appear only in two cases:
 * - In a file, where it separates the filename from the extension
 * - In the "." and ".." of the current directory
 *
 * @param path Pointer for the path to be validated
 * @param isFile Pointer to a flag that tells if it's a file or a directory
 * @param curPos Index position on path
 * @param nameSize Size of the path being passed
 *
 * @return 0 if is valid and -1 otherwise. In the case of a file the isFile will
 * also be modified
 */
static int
  validated_dot(const char *path, bool *isFile, size_t curPos, int nameSize)
{
  // If a "." is found we need to verify if its a file or a . directory
  if (path[curPos] == '.') {
    // Hidden files are not accepted
    if (nameSize == 0 && path[curPos + 1] != '/' && path[curPos + 1] != '.'
        && path[curPos + 1] != '\0')
      return -1;

    // Files can have "." in the middle(and only them)
    if (nameSize != 0 && path[curPos - 1] != '.' && path[curPos - 1] != '/') {
      if (*isFile)
        return -1;
      else
        *isFile = true;
    }
  }
  return 0;
}

/**
 * @brief Validates a path
 *
 * There is some specifications in the FAT of how does a path need to be.
 * There is also the case of the specification by the OS, in here we are
 * following only the first one.
 *
 * @param path Pointer for the path to validated
 * @param pathLen Lenght of the path
 *
 * @return 0 if the path is valid, and -1 otherwise
 */
static int path_validated(const char *path, size_t pathLen)
{
  if (path == NULL)
    return -1;

  int slashNum = 0;
  int nameSize = 0;
  bool isFile = false;
  for (size_t i = 0; i < pathLen; i++, nameSize++) {
    // Verify the case where the filepath is "//"
    if (path[i] == '/') {
      nameSize = 0;
      slashNum++;
      if (slashNum >= 2)
        return -1;
    } else {
      slashNum = 0;
      if (!isalnum(path[i]) && !isspecial(path[i]))
        return -1;

      // Files and directories have different name lengh limitations.
      if (isFile) {
        if (nameSize > FILENAME_SIZE + 1)
          return -1;
      } else {
        if (nameSize > NAME_SIZE + 1)
          return -1;
      }
    }

    if (validated_dot(path, &isFile, i, nameSize) < 0)
      return -1;
  }
  return 0;
}

/**
 * @brief Formats the token of a directory
 *
 * @param token Pointer for the token to be formatted
 * @param tokenLen Lenght of the token to be formatted
 *
 * @return A newly allocated formatted token if the operation was successful,
 * and NULL otherwise.
 */
static char *directory_format(const char *token, size_t tokenLen)
{
  char *formatted = (char *)calloc(FILENAME_SIZE + 1, sizeof(char));
  if (formatted == NULL)
    return NULL;

  for (size_t i = 0, j = 0; i < FILENAME_SIZE; i++) {
    if (j < tokenLen) {
      formatted[i] = (char)toupper(token[j]);
      j++;
    } else {
      formatted[i] = ' ';
    }
  }

  return formatted;
}

/**
 * @brief Formats the token of a file name
 *
 * Files are formatted in two parts the name of the file and the extension.
 * The division of both are made by the dot.
 *
 * @param token Pointer for the token to be formatted
 * @param dot The position of the dot in the token
 * @param tokenLen Lenght of the token to be formatted
 *
 * @return A newly allocated formatted token if the operation was successful,
 * and NULL otherwise.
 */
static char *file_format(const char *token, const char *dot, size_t tokenLen)
{
  char *formatted = (char *)calloc(FILENAME_SIZE + 1, sizeof(char));
  if (formatted == NULL)
    return NULL;

  // Copy the name of the file
  for (size_t i = 0, j = 0; i < 8; i++) {
    if (token + j == dot) {
      formatted[i] = ' ';
    } else {
      formatted[i] = (char)toupper(token[j]);
      j++;
    }
  }

  // Copy the extension of the file
  for (size_t i = FILENAME_SIZE - 1, j = tokenLen - 1; i >= NAME_SIZE; i--) {
    if (token + j == dot) {
      formatted[i] = ' ';
    } else {
      formatted[i] = (char)toupper(token[j]);
      j--;
    }
  }

  return formatted;
}

/**
 * @brief Formats the token to the FAT name style
 *
 * The type of the FAT is determined by the count of clusters on the volume and
 * nothing else.
 *
 * @param token Pointer for the token to be formatted
 *
 * @return A newly allocated formatted token if the operation was successful,
 * and NULL otherwise.
 */
static char *token_format(const char *token)
{
  if (token == NULL)
    return NULL;

  // Handle special case "." and ".."
  size_t tokenLen = strlen(token);
  if (tokenLen == 1 && token[0] == '.') {
    char *dotToken = strndup(DOT_FILENAME, 11);
    if (dotToken == NULL)
      return NULL;

    return dotToken;
  } else if (tokenLen == 2 && token[0] == '.' && token[1] == '.') {
    char *dotToken = strndup(DOTDOT_FILENAME, 11);
    if (dotToken == NULL)
      return NULL;

    return dotToken;
  }

  char *dot = strchr(token, '.');

  // If is a directory we can copy the chars directly, adding spaces in the end
  if (dot == NULL)
    return directory_format(token, tokenLen);

  return file_format(token, dot, tokenLen);
}

/******************************************************************************
 *                              PUBLIC FUNCTIONS
 ******************************************************************************/

// TODO: Add support for the root directory
PathParser *parser_create(const char *path)
{
  if (path == NULL)
    return NULL;

  if (strcmp(path, "") == 0 || strcmp(path, "/") == 0)
    return NULL;

  size_t pathLen = strnlen(path, PATH_MAX);
  if (pathLen > PATH_MAX)
    return NULL;

  if (path_validated(path, pathLen) < 0)
    return NULL;

  PathParser *parser = (PathParser *)malloc(sizeof(*parser));
  if (parser == NULL)
    return NULL;

  char *dup = strndup(path, pathLen);
  if (dup == NULL) {
    free(parser);
    return NULL;
  }

  parser->path = dup;
  parser->saveptr = NULL;
  parser->curToken = NULL;
  if (parser_token(parser) < 0)
    parser_delete(&parser);

  return parser;
}

void parser_delete(PathParser **parser)
{
  if (*parser == NULL)
    return;

  free((*parser)->path);
  free((*parser)->curToken);
  free(*parser);
  *parser = NULL;
}

int parser_token(PathParser *parser)
{
  if (parser == NULL)
    return -1;

  char *token = NULL;
  if (parser->curToken == NULL && parser->saveptr == NULL)
    token = strtok_r(parser->path, "/", &parser->saveptr);
  else
    token = strtok_r(NULL, "/", &parser->saveptr);

  if (token == NULL)
    return 1;

  char *formatted = token_format(token);
  if (formatted == NULL)
    return -1;

  free(parser->curToken);
  parser->curToken = formatted;

  return 0;
}
