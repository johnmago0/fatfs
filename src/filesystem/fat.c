#include "filesystem/fat.h"
#include "filesystem/bpb.h"
#include "io/volume.h"

#include <assert.h>
#include <stdlib.h>

#define NUM_FATS 2

/******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************/

/**
 * @brief Reads sector of the FAT that has the specified cluster
 *
 * @param cluster The cluster number that needs to be readed.
 * @param fat Number of the fat that is going to be readed.
 *
 * @return A newly allocated buffer, or NULL otherwise.
 */
static void *fat_buffer_read(DWORD cluster, DWORD fat)
{
  if (cluster > bpb_count_of_clusters()) {
    return NULL;
  }

  // TODO: Is this the best way to deal with this?
  // Allocates the buffer with one byte more.
  // I'm doing this to avoid invalid read and write operations when in a out of
  // bound array
  void *buffer = calloc(1, bpb_bytes_per_sec() + 1);
  if (buffer == NULL) {
    return NULL;
  }

  DWORD offset = bpb_fat_sec_num(bpb_fat_offset(cluster)) * bpb_bytes_per_sec();
  offset += fat * bpb_fat_size();

  if (volume_read(offset, buffer, bpb_bytes_per_sec()) < 0) {
    free(buffer);
    return NULL;
  }

  return buffer;
}

/**
 * @brief Write sector of the FAT that has the specified cluster
 *
 * @param cluster The cluster number that needs to be writed.
 * @param fat Number of the fat that is going to be writed.
 *
 * @return 0 if the it could be writted and -1 otherwise.
 */
static int fat_buffer_write(void *buffer, DWORD cluster, DWORD fat)
{
  if (cluster > bpb_count_of_clusters()) {
    return -1;
  }

  DWORD offset = bpb_fat_sec_num(bpb_fat_offset(cluster)) * bpb_bytes_per_sec();
  offset += fat * bpb_fat_size();

  if (volume_write(offset, buffer, bpb_bytes_per_sec()) < 0) {
    return -1;
  }

  return 0;
}

/**
 * @brief Searches for a empty cluster inside the FAT structure
 *
 * @param cluster Pointer for the cluster that is empty
 * @param startCluster The first cluster to start the search.
 *
 * @return 0 if the cluster was found and -1 otherwise.
 */
static int fat_search_empty(DWORD *cluster, DWORD startCluster)
{
  if (startCluster > bpb_count_of_clusters()) {
    return -1;
  }

  DWORD iterator = startCluster;
  DWORD res = 0;
  do {
    if (fat_read(iterator, 0, (WORD *)&res) < 0) {
      return -1;
    }

    if (res == 0) {
      *cluster = iterator;
      return 0;
    }

    iterator = (iterator + 1) % bpb_fat_size();
  } while (iterator != startCluster);

  return -1;
}

/**
 * @brief Deallocate a chain that could not be completed allocated
 *
 * @param cluster First cluster of the chain
 * @param nclusters The number of clusters that were bad allocated.
 *
 * @return 0 if the deallocation was successful and -1 otherwise.
 */
static int fat_deallocate_incomplete_chain(DWORD cluster, DWORD nclusters)
{
  DWORD nextCluster = 0;

  do {
    if (fat_read(cluster, 0, (WORD *)&nextCluster) < 0) {
      return -1;
    }

    if (fat_write(cluster, 0) < 0) {
      return -1;
    }

    cluster = nextCluster;
    nclusters--;
  } while (nclusters != 0);

  return 0;
}

/******************************************************************************
 * PUBLIC FUNCTIONS
 ******************************************************************************/

int fat_read(DWORD cluster, BYTE fat, WORD *dest)
{
  if (fat >= NUM_FATS) {
    return -1;
  }

  if (cluster > bpb_count_of_clusters()) {
    return -1;
  }

  void *SecBuffer = fat_buffer_read(cluster, fat);
  if (SecBuffer == NULL) {
    return -1;
  }

  WORD fatEntryOffset = (WORD)bpb_fat_entry_offset(bpb_fat_offset(cluster));

  // NOTE: Currently reading only the first FAT. How should I proceed in the
  // case of a corruption?
  WORD fat12ClusEntryVal = *((WORD *)&SecBuffer[fatEntryOffset]);

  // This method of write only works in the case of FAT12
  if (cluster & 0x0001) {
    // Current cluster is ODD
    fat12ClusEntryVal >>= 4;
  } else {
    // Current cluster is EVEN
    fat12ClusEntryVal &= 0x0FFF;
  }

  *dest = fat12ClusEntryVal;
  free(SecBuffer);
  return 0;
}

int fat_write(DWORD cluster, WORD entryVal)
{
  if (cluster > bpb_count_of_clusters()) {
    return -1;
  }

  for (DWORD i = 0; i < NUM_FATS; i++) {
    void *SecBuffer = fat_buffer_read(cluster, 0);
    if (SecBuffer == NULL) {
      return -1;
    }

    WORD fatEntryOffset = (WORD)bpb_fat_entry_offset(bpb_fat_offset(cluster));

    DWORD fat12ClusEntryVal = entryVal;
    // This method of write only works in the case of FAT12
    if (cluster & 0x0001) {
      // Current cluster is ODD
      fat12ClusEntryVal <<= 4;
      *((WORD *)&SecBuffer[fatEntryOffset]) &= 0x000F;
    } else {
      // Current cluster is EVEN
      fat12ClusEntryVal &= 0x0FFF;
      *((WORD *)&SecBuffer[fatEntryOffset]) &= 0xF000;
    }

    *((WORD *)&SecBuffer[fatEntryOffset]) |= fat12ClusEntryVal;

    if (fat_buffer_write(SecBuffer, cluster, i) < 0) {
      free(SecBuffer);
      return -1;
    }

    free(SecBuffer);
  }

  return 0;
}

int fat_allocate_chain(DWORD nclusters, DWORD *cluster)
{
  if (nclusters > bpb_count_of_clusters()) {
    return -1;
  }

  DWORD curCluster = 0;
  DWORD nextCluster = 0;

  if (fat_search_empty(&curCluster, 0) < 0) {
    return -1;
  }

  DWORD firstCluster = curCluster;
  DWORD numClusters = nclusters;

  do {
    numClusters--;
    if (numClusters == 0) {
      if (fat_write(curCluster, (WORD)bpb_fat_eof()) < 0) {
        fat_deallocate_incomplete_chain(firstCluster, nclusters - numClusters);
        return -1;
      }

      *cluster = firstCluster;
      return 0;
    } else {
      if (fat_search_empty(&nextCluster, curCluster + 1) < 0) {
        fat_deallocate_incomplete_chain(firstCluster, nclusters - numClusters);
        return -1;
      }

      if (fat_write(curCluster, (WORD)nextCluster) < 0) {
        fat_deallocate_incomplete_chain(firstCluster, nclusters - numClusters);
        return -1;
      }

      curCluster = nextCluster;
    }
  } while (numClusters != 0);

  *cluster = firstCluster;
  return 0;
}

int fat_deallocate_chain(DWORD cluster)
{
  DWORD nextCluster = 0;
  DWORD numReads = 0;

  do {
    if (fat_read(cluster, 0, (WORD *)&nextCluster) < 0) {
      return -1;
    }

    if (fat_write(cluster, 0) < 0) {
      return -1;
    }

    cluster = nextCluster;
    numReads++;

    if (numReads > bpb_count_of_clusters()) {
      return -1;
    }
  } while (cluster != bpb_fat_eof());

  return 0;
}

int fat_allocate_cluster(DWORD cluster, DWORD *newCluster)
{
  if (cluster > bpb_fat_eof()) {
    return -1;
  }

  DWORD nextCluster = 0;
  DWORD numReads = 0;

  do {
    if (fat_read(cluster, 0, (WORD *)&nextCluster) < 0) {
      return -1;
    }

    if (nextCluster == bpb_fat_eof()) {
      if (fat_search_empty(&nextCluster, cluster) < 0) {
        return -1;
      }

      if (fat_write(cluster, (WORD)nextCluster) < 0) {
        return -1;
      }

      if (fat_write(nextCluster, (WORD)bpb_fat_eof()) < 0) {
        return -1;
      }

      *newCluster = nextCluster;
      return 0;
    }

    numReads++;
    cluster = nextCluster;
  } while (numReads <= bpb_count_of_clusters());

  return -1;
}

int fat_deallocate_cluster(DWORD cluster)
{
  if (cluster > bpb_fat_eof()) {
    return -1;
  }

  DWORD curCluster = 0;

  if (fat_read(cluster, 0, (WORD *)&curCluster) < 0) {
    return -1;
  }

  if (curCluster == bpb_fat_eof()) {
    return -1;
  }

  DWORD nextCluster = 0;
  DWORD auxCluster = 0;
  DWORD numReads = 0;
  do {
    if (fat_read(curCluster, 0, (WORD *)&nextCluster) < 0) {
      return -1;
    }

    if (fat_read(nextCluster, 0, (WORD *)&auxCluster) < 0) {
      return -1;
    }

    if (auxCluster == bpb_fat_eof()) {
      if (fat_write(curCluster, (WORD)bpb_fat_eof()) < 0) {
        return -1;
      }

      if (fat_write(nextCluster, 0) < 0) {
        return -1;
      }

      return 0;
    }

    numReads++;
    curCluster = nextCluster;
    nextCluster = auxCluster;
  } while (numReads < bpb_count_of_clusters());

  return -1;
}
