#include "filesystem/fs_api.h"
#include "filesystem/bpb.h"
#include "filesystem/directory.h"
#include "filesystem/fat.h"
#include "io/volume.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

/******************************************************************************
 * Private Functions
 ******************************************************************************/

/**
 * @brief Prints a Directory. Use for debugging purposes.
 *
 * @param dir The directory pointer to be printed.
 */
static void print_dentry(const Directory *dir)
{
  if (dir == NULL) {
    printf("Directory structure is NULL");
    return;
  }

  printf("DIR_Name: ");
  for (int i = 0; i < 11; i++) {
    printf("%c", dir->DIR_Name[i]);
  }

  printf("\n");
  printf("DIR_Attr: 0x%x\n", dir->DIR_Attr);
  printf("DIR_NTRes: %d\n", dir->DIR_NTRes);
  printf("DIR_CrtTimeTenth: %d\n", dir->DIR_CrtTimeTenth);
  printf("DIR_CrtTime: %d\n", dir->DIR_CrtTime);
  printf("DIR_CrtDate: %d\n", dir->DIR_CrtDate);
  printf("DIR_LstAccDate: %d\n", dir->DIR_LstAccDate);
  printf("DIR_FstClusHI: %d\n", dir->DIR_FstClusHI);
  printf("DIR_WrtTime: %d\n", dir->DIR_WrtTime);
  printf("DIR_WrtDate: %d\n", dir->DIR_WrtDate);
  printf("DIR_FstClusLO: %d\n", dir->DIR_FstClusLO);
  printf("DIR_FileSize: %d\n", dir->DIR_FileSize);
}

/**
 * @brief Creates a stat_t pointer from a directory entry
 *
 * @param dir The directory pointer that is going to be readed.
 *
 * @return A newly allocated stat_t struct, or NULL otherwise.
 */
static stat_t *stat_create(const Directory *dir)
{
  if (dir == NULL) {
    return NULL;
  }

  stat_t *stat = (stat_t *)malloc(sizeof(*stat));
  if (stat == NULL) {
    return NULL;
  }

  strncpy(stat->name, (char *)dir->DIR_Name, 11);
  stat->attr = dir->DIR_Attr;
  stat->createTime = dir->DIR_CrtTime;
  stat->writeTime = dir->DIR_WrtTime;
  stat->createDate = dir->DIR_CrtDate;
  stat->writeDate = dir->DIR_WrtDate;
  stat->lastAccDate = dir->DIR_LstAccDate;
  stat->cluster = directory_cluster(dir);
  stat->fileSize = dir->DIR_FileSize;

  return stat;
}

/**
 * @brief Deallocates the stat pointer
 *
 * @param stat The stat pointer that is going to be deallocated.
 */
static void stat_delete(stat_t **stat)
{
  free(*stat);
  *stat = NULL;
}

/**
 * @brief Creates a dir_t pointer from its sector
 *
 * @param sector The sector where the directory can be found.
 *
 * @return A newly allocated dir_t struct, or NULL otherwise.
 */
static dir_t *dir_create(DWORD sector)
{
  dir_t *dir = (dir_t *)malloc(sizeof(*dir));
  if (dir == NULL) {
    return NULL;
  }

  dir->isRoot = 0;
  dir->curSector = sector;

  Directory *openDir = directory_fetch(dir->curSector);
  if (openDir == NULL || !(openDir->DIR_Attr & (BYTE)ATTR_DIRECTORY)) {
    free(dir);
    return NULL;
  }

  if (directory_search("..", dir->curSector, &dir->dotdotSector) < 0) {
    free(openDir);
    free(dir);
    return NULL;
  }

  dir->stat = stat_create(openDir);
  if (dir->stat == NULL) {
    free(openDir);
    free(dir);
    return NULL;
  }

  free(openDir);
  return dir;
}

/**
 * @brief Creates a file_t pointer from its sector
 *
 * @param sector The sector where the file can be found.
 *
 * @return A newly allocated file_t struct, or NULL otherwise.
 */
static file_t *file_create(DWORD sector)
{
  file_t *file = (file_t *)malloc(sizeof(*file));
  if (file == NULL) {
    return NULL;
  }

  file->byte = 0;
  file->sector = sector;

  Directory *openDir = directory_fetch(file->sector);
  if (openDir == NULL || (openDir->DIR_Attr & (BYTE)ATTR_DIRECTORY)) {
    free(file);
    return NULL;
  }

  file->stat = stat_create(openDir);
  if (file->stat == NULL) {
    free(openDir);
    free(file);
    return NULL;
  }

  file->cluster = file->stat->cluster;
  free(openDir);
  return file;
}

/**
 * @brief Creates a file_t pointer from its sector
 *
 * @param sector The sector where the file can be found.
 *
 * @return A newly allocated file_t struct, or NULL otherwise.
 */
static int file_seek_set(file_t *file, long offset)
{
  file->byte = 0;
  file->cluster = file->stat->cluster;

  if (offset < 0) {
    return 0;
  }

  if (offset > bpb_cluster_size()) {
    DWORD iterOffset = offset;
    DWORD curCluster = file->cluster;
    while (iterOffset > bpb_cluster_size()) {
      if (fat_read(curCluster, 0, (WORD *)&curCluster) < 0) {
        return -1;
      }

      if (curCluster == bpb_fat_eof()) {
        file->byte = 0;
        return 0;
      }

      iterOffset -= bpb_cluster_size();
      file->cluster = curCluster;
    }

    if (offset > file->stat->fileSize) {
      file->byte = 0;
    } else {
      file->byte = (DWORD)iterOffset;
    }
  } else {
    file->byte = (DWORD)offset;
  }

  return 0;
}

/******************************************************************************
 * Public Functions
 ******************************************************************************/

int fs_mount(const char *volume)
{
  if (volume_open(volume) < 0) {
    return -1;
  }

  if (bpb_open() < 0) {
    volume_close();
    return -1;
  }

  return 0;
}

void fs_umount()
{
  bpb_close();
  volume_close();
}

file_t *fs_open(const char *path)
{
  if (path == NULL) {
    return NULL;
  }

  DWORD sector = 0;

  if (directory_root_search(path, &sector) < 0) {
    return NULL;
  }

  return file_create(sector);
}

dir_t *fs_opendir(const char *path)
{
  if (path == NULL) {
    return NULL;
  }

  if (strcmp(path, "/") == 0) {
    dir_t *dir = (dir_t *)calloc(1, sizeof(dir_t));
    if (dir == NULL) {
      return NULL;
    }

    dir->isRoot = 1;
    return dir;
  }

  DWORD sector = 0;
  if (directory_root_search(path, &sector) < 0) {
    return NULL;
  }

  return dir_create(sector);
}

void fs_closedir(dir_t *dir)
{
  if (dir->isRoot == 1) {
    free(dir);
    return;
  }

  stat_delete(&dir->stat);
  free(dir);
}

void fs_close(file_t *file)
{
  stat_delete(&file->stat);
  free(file);
}

stat_t *fs_stat(file_t *file)
{
  if (file == NULL) {
    return NULL;
  }

  stat_t *stat = (stat_t *)malloc(sizeof(stat_t));
  if (stat == NULL) {
    return NULL;
  }

  memcpy(stat, file->stat, sizeof(stat_t));
  return stat;
}

stat_t *fs_statdir(dir_t *dir)
{
  if (dir == NULL) {
    return NULL;
  }

  stat_t *stat = (stat_t *)malloc(sizeof(stat_t));
  if (stat == NULL) {
    return NULL;
  }

  memcpy(stat, dir->stat, sizeof(stat_t));
  return stat;
}

int fs_seek(file_t *file, long offset, seek_t seek)
{
  switch (seek) {
  case SET:
    return file_seek_set(file, offset);
  case END:
    if (offset < 0) {
      offset = file->stat->fileSize + offset;
    } else {
      offset = file->stat->fileSize;
    }
    return file_seek_set(file, offset);
  case CUR:
  default:
    assert(0 && "Invalid seek");
  }

  return -1;
}

ssize_t fs_read(file_t *file, void *buffer, size_t size)
{
  if (file == NULL || buffer == NULL) {
    return -1;
  }

  return directory_read(file, buffer, size);
}

ssize_t fs_write(file_t *file, void *buffer, size_t size)
{
  if (file == NULL || buffer == NULL) {
    return -1;
  }

  return directory_write(file->sector, buffer, size);
}

file_t *fs_mkfile(const char *path, BYTE attr)
{
  if (path == NULL) {
    return NULL;
  }

  if (strcmp("/", path) == 0) {
    return NULL;
  }

  if ((attr & (BYTE)ATTR_VOLUME_ID) && (attr & (BYTE)ATTR_SYSTEM)) {
    return NULL;
  }

  DWORD sector = 0;
  if (directory_file_allocate(path, attr | (BYTE)ATTR_ARCHIVE, &sector) < 0) {
    return NULL;
  }

  return file_create(sector);
}

dir_t *fs_mkdir(const char *path, BYTE attr)
{
  if (path == NULL) {
    return NULL;
  }

  if (strcmp("/", path) == 0) {
    return NULL;
  }

  if ((attr & (BYTE)ATTR_VOLUME_ID) && (attr & (BYTE)ATTR_SYSTEM)) {
    return NULL;
  }

  DWORD sector = 0;
  if (directory_dir_allocate(path, attr | (BYTE)ATTR_DIRECTORY, &sector) < 0) {
    return NULL;
  }

  return dir_create(sector);
}
