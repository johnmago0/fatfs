#include "filesystem/bpb.h"
#include "io/volume.h"

#include <assert.h>
#include <stdio.h>

#define EOF12 0x0FF8
#define EOF16 0x0FFF8
#define EOF32 0x0FFFFFF8

static BPB *bios = NULL;
static int FatType = 0;
static DWORD FATSz = 0;
static DWORD FATByteSz = 0;
static DWORD TotSec = 0;
static DWORD FATSec = 0;
static DWORD ClusSz = 0;
static DWORD FstDataSec = 0;
static DWORD RootDirSec = 0;
static DWORD FATEOF = 0;
static DWORD CountOfClusters = 0;

/******************************************************************************
 * Private Functions
 ******************************************************************************/

/**
 * @brief Gets the type of the FAT filesystem
 *
 * The type of the FAT is determined by the count of clusters on the volume and
 * nothing else.
 */
static void fat_type()
{
  assert("BPB should not be NULL" && bios != NULL);
  DWORD RootDirSectors =
    ((bios->BPB_RootEntCnt * 32) + (bios->BPB_BytsPerSec - 1))
    / bios->BPB_BytsPerSec;

  if (bios->BPB_FATSz16 != 0) {
    FATSz = bios->BPB_FATSz16;
  } else {
    FATSz = bios->extension.block32.BPB_FATSz32;
  }

  if (bios->BPB_TotSec16 != 0) {
    TotSec = bios->BPB_TotSec16;
  } else {
    TotSec = bios->BPB_TotSec32;
  }

  DWORD DataSec =
    TotSec
    - (bios->BPB_RsvdSecCnt + (bios->BPB_NumFATs * FATSz) + RootDirSectors);
  CountOfClusters = DataSec / bios->BPB_SecPerClus;

  if (CountOfClusters < 4085) {
    FatType = FAT12;
  } else if (CountOfClusters < 65525) {
    printf("FAT16 is not currently supported\n");
    exit(-1);
  } else {
    printf("FAT32 is not currently supported\n");
    exit(-1);
  }
}

/**
 * @brief Initialized the global variables used during calculations
 */
static void bpb_init_globals()
{
  assert("BPB should not be NULL" && bios != NULL);
  // Determine the size of the FAT structure in bytes
  FATByteSz = FATSz * bios->BPB_BytsPerSec;

  // Determine the sector where the FAT is located
  FATSec = bios->BPB_RsvdSecCnt * bios->BPB_BytsPerSec;

  // Determine the size of a cluster in bytes
  ClusSz = bios->BPB_SecPerClus * bios->BPB_BytsPerSec;

  // Determine the Root dir first sector
  // The following is only valid for the FAT12/16 volumes.
  // FAT32 puts it's cluster in the one of the cluster chains.
  DWORD firstRootDirSec =
    bios->BPB_RsvdSecCnt + (bios->BPB_NumFATs * bios->BPB_FATSz16);
  RootDirSec = firstRootDirSec * bios->BPB_BytsPerSec;

  // Determines the first sector that has data
  DWORD rootDirCntSec =
    ((bios->BPB_RootEntCnt * 32) + (bios->BPB_BytsPerSec - 1))
    / bios->BPB_BytsPerSec;
  FstDataSec =
    bios->BPB_RsvdSecCnt + (bios->BPB_NumFATs * FATSz) + rootDirCntSec;

  // Determine the EOF of the type of the FAT
  switch (FatType) {
  case FAT12:
    FATEOF = EOF12;
    break;
  case FAT16:
    FATEOF = EOF16;
    break;
  case FAT32:
    FATEOF = EOF32;
    break;
  default:
    assert("FAT Type not defined" && 0);
  }
}

/******************************************************************************
 * Public Functions
 ******************************************************************************/

int bpb_open()
{
  bios = (BPB *)malloc(sizeof(*bios));
  if (volume_read(0, bios, sizeof(*bios)) < 0) {
    return -1;
  }

  fat_type();
  bpb_init_globals();
  return 0;
}

void bpb_close() { free(bios); }

DWORD bpb_fat_size()
{
  assert("BPB should not be NULL" && bios != NULL);
  return FATByteSz;
}

DWORD bpb_fat_eof()
{
  assert("BPB should not be NULL" && bios != NULL);
  return FATEOF;
}

DWORD bpb_fat_offset(DWORD cluster)
{
  assert("BPB should not be NULL" && bios != NULL);
  DWORD fatOffset = 0;
  switch (FatType) {
  case FAT12:
    fatOffset = cluster + (cluster / 2);
    break;
  case FAT16:
    fatOffset = cluster * 2;
    break;
  case FAT32:
    fatOffset = cluster * 4;
    break;
  default:
    assert("FAT Type not defined" && 0);
  };
  return fatOffset;
}

DWORD bpb_fat_entry_offset(DWORD offset)
{
  assert("BPB should not be NULL" && bios != NULL);
  return offset % bios->BPB_BytsPerSec;
}

DWORD bpb_fat_sec_num(DWORD offset)
{
  assert("BPB should not be NULL" && bios != NULL);
  return bios->BPB_RsvdSecCnt + (offset / bios->BPB_BytsPerSec);
}

DWORD bpb_count_of_clusters()
{
  assert("BPB should not be NULL" && bios != NULL);
  return CountOfClusters;
}

DWORD bpb_root_entry_count()
{
  assert("BPB should not be NULL" && bios != NULL);
  // The following is only valid for the FAT12/16 volumes.
  // FAT32 puts it's cluster in the one of the cluster chains.
  return bios->BPB_RootEntCnt;
}

DWORD bpb_root_sector()
{
  assert("BPB should not be NULL" && bios != NULL);
  return RootDirSec;
}

DWORD bpb_cluster_to_sector(DWORD cluster)
{
  assert("BPB should not be NULL" && bios != NULL);
  return (((cluster - 2) * bios->BPB_SecPerClus) + FstDataSec)
         * bios->BPB_BytsPerSec;
}

DWORD bpb_cluster_size()
{
  assert("BPB should not be NULL" && bios != NULL);
  return ClusSz;
}

DWORD bpb_bytes_per_sec()
{
  assert("BPB should not be NULL" && bios != NULL);
  return bios->BPB_BytsPerSec;
}
