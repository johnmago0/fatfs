#include "filesystem/directory.h"
#include "filesystem/bpb.h"
#include "filesystem/fat.h"
#include "io/volume.h"
#include "utils/pathparser.h"
#include "utils/timestamp.h"

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************
 *                              PRIVATE FUNCTIONS
 ******************************************************************************/

/**
 * @brief Creates a directory.
 *
 * @param name The name of the file.
 * @param attributes The attributes of the file.
 *
 * @return A newly allocated FatBuffer structure, or NULL if an error occurs.
 */
static Directory *
  directory_create(const char *name, BYTE attributes, DWORD cluster)
{
  if (name == NULL) {
    return NULL;
  }

  Directory *dir = calloc(1, sizeof(*dir));
  if (dir == NULL) {
    return NULL;
  }

  strncpy((char *)dir->DIR_Name, name, 11);
  dir->DIR_Attr = attributes;

  WORD timestamp = timestamp_current_time();
  WORD datestamp = timestamp_current_date();

  dir->DIR_CrtTime = timestamp;
  dir->DIR_CrtDate = datestamp;
  dir->DIR_WrtTime = timestamp;
  dir->DIR_WrtDate = datestamp;
  dir->DIR_LstAccDate = timestamp;
  dir->DIR_CrtTimeTenth = timestamp_current_milliseconds();

  dir->DIR_FstClusHI = (WORD)(cluster >> 16);
  dir->DIR_FstClusLO = (WORD)(cluster);

  return dir;
}

/**
 * @brief Creates a directory.
 *
 * @param name The name of the file.
 * @param attributes The attributes of the file.
 *
 * @return A newly allocated FatBuffer structure, or NULL if an error occurs.
 */
static Directory *directory_root_create()
{
  Directory *dir = calloc(1, sizeof(*dir));
  if (dir == NULL) {
    return NULL;
  }

  strncpy((char *)dir->DIR_Name, DOTDOT_FILENAME, 11);
  dir->DIR_Attr = ATTR_SYSTEM & ATTR_VOLUME_ID;

  dir->DIR_CrtTime = (WORD)-1;
  dir->DIR_CrtDate = (WORD)-1;
  dir->DIR_WrtTime = (WORD)-1;
  dir->DIR_WrtDate = (WORD)-1;
  dir->DIR_LstAccDate = (WORD)-1;
  dir->DIR_CrtTimeTenth = (BYTE)-1;

  return dir;
}

/**
 * @brief Searches for a file by its name in the root directory.
 *
 * @param filename The name of the file.
 * @param sector Pointer for the sector where the directory is allocated.
 *
 * @return 0 if the file was found, and -1 otherwise.
 */
static int rootdir_search(const char *filename, DWORD *sector)
{
  Directory *dir = (Directory *)malloc(sizeof(*dir));
  if (dir == NULL) {
    return -1;
  }

  int res = -1;
  DWORD rootOffset = bpb_root_sector();
  DWORD rootEntCount = bpb_root_entry_count();
  for (WORD i = 0; i < rootEntCount; i++, rootOffset += sizeof(*dir)) {
    if (volume_read(rootOffset, dir, sizeof(*dir)) != 0) {
      goto exit;
    }

    if (dir->DIR_Name[0] == LAST_ENTRY) {
      goto exit;
    }

    if (dir->DIR_Name[0] == FREE_ENTRY) {
      continue;
    }

    if (strncmp((char *)dir->DIR_Name, filename, 11) == 0) {
      res = 0;
      break;
    }
  }

  *sector = rootOffset;
exit:
  free(dir);
  return res;
}

/**
 * @brief Searches for a file by its name in a subdirectory.
 *
 * @param filename The name of the file.
 * @param dirCluster The cluster of the directory that is going to be searched.
 * @param sector Pointer for the sector where the directory is allocated.
 *
 * @return 0 if the file was found, and -1 otherwise.
 */
static int subdir_search(const char *filename, DWORD dirCluster, DWORD *sector)
{
  Directory *dir = (Directory *)malloc(sizeof(*dir));
  if (dir == NULL) {
    return -1;
  }

  int res = -1;
  DWORD subOffset = bpb_cluster_to_sector(dirCluster);
  while (1) {
    if (volume_read(subOffset, dir, sizeof(*dir)) != 0) {
      goto exit;
    }

    if (dir->DIR_Name[0] == LAST_ENTRY) {
      goto exit;
    }

    if (dir->DIR_Name[0] == FREE_ENTRY) {
      continue;
    }

    if (strncmp((char *)dir->DIR_Name, filename, 11) == 0) {
      res = 0;
      break;
    }

    subOffset += sizeof(*dir);
  }

  *sector = subOffset;
exit:
  free(dir);
  return res;
}

/**
 * @brief Searches a empty entry in the root directory.
 *
 * @param filename The name of the file.
 * @param sector Pointer for the sector where the empty entry is.
 *
 * @return 0 if the entry was found, and -1 otherwise.
 */
static int rootdir_search_empty(const char *filename, DWORD *sector)
{
  Directory *dir = (Directory *)malloc(sizeof(*dir));
  if (dir == NULL) {
    return -1;
  }

  int res = -1;
  DWORD rootOffset = bpb_root_sector();
  DWORD rootEntCount = bpb_root_entry_count();
  for (WORD i = 0; i < rootEntCount; i++, rootOffset += sizeof(*dir)) {
    if (volume_read(rootOffset, dir, sizeof(*dir)) != 0) {
      break;
    }

    if (dir->DIR_Name[0] == LAST_ENTRY) {
      res = 0;
      *sector = rootOffset;
      break;
    }

    if (dir->DIR_Name[0] == FREE_ENTRY) {
      res = 0;
      *sector = rootOffset;
      break;
    }

    if (strncmp((char *)dir->DIR_Name, filename, 11) == 0) {
      res = -1;
      break;
    }
  }

  free(dir);
  return res;
}

/**
 * @brief Searches a empty entry in the subdirectory.
 *
 * @param filename The name of the file.
 * @param dirCluster Cluster of the directory being searched.
 * @param sector Pointer for the sector where the empty entry is.
 *
 * @return 0 if the entry was found, and -1 otherwise.
 */
static int
  subdir_search_empty(const char *filename, DWORD dirCluster, DWORD *sector)
{
  Directory *dir = (Directory *)malloc(sizeof(*dir));
  if (dir == NULL) {
    return -1;
  }

  int res = -1;
  DWORD subOffset = bpb_cluster_to_sector(dirCluster);
  while (1) {
    if (volume_read(subOffset, dir, sizeof(*dir)) != 0) {
      break;
    }

    if (dir->DIR_Name[0] == LAST_ENTRY) {
      res = 0;
      *sector = subOffset;
      break;
    }

    if (dir->DIR_Name[0] == FREE_ENTRY) {
      res = 0;
      *sector = subOffset;
      break;
    }

    if (strncmp((char *)dir->DIR_Name, filename, 11) == 0) {
      res = -1;
      break;
    }

    subOffset += sizeof(*dir);
  }

  *sector = subOffset;
  free(dir);
  return res;
}

/**
 * @brief Reads the data of a directory.
 *
 * @param buffer The buffer were the data will be allocated.
 * @param size The size of the buffer.
 * @param offset The offset were the data is in the disk.
 * @param cluster The first cluster of the data.
 *
 * @return The number of data readed from the disk, or a negative number if
 * something went wrong.
 */
static ssize_t
  directory_read_data(file_t *file, void *buffer, size_t size, DWORD offset)
{
  DWORD numClusters = 0;
  DWORD clusterSize = bpb_cluster_size();
  DWORD clusterGuard = (DWORD)CEILING(size, clusterSize);
  ssize_t retValue = 0;

  do {
    if (numClusters > clusterGuard) {
      return -1;
    }

    // Read the data in clusters, until we found a "broken" FileSize.
    if (size > clusterSize) {
      if (volume_read(
            offset, buffer + ((size_t)(clusterSize * numClusters)), clusterSize)
          != 0) {
        return -1;
      }

      retValue += clusterSize;
      offset += clusterSize;
      size -= clusterSize;
      file->byte = 0;
    } else {
      if (volume_read(
            offset, buffer + ((size_t)(clusterSize * numClusters)), size)
          != 0) {
        return -1;
      }

      retValue += size;
      file->byte += size;
      break;
    }

    if (fat_read(file->cluster, 0, (WORD *)&file->cluster) < 0) {
      break;
    }

    numClusters++;
  } while (file->cluster <= bpb_fat_eof());

  if (file->cluster != bpb_fat_eof()) {
    file->sector = bpb_cluster_to_sector(file->cluster);
  } else {
    file->sector = 0;
  }

  return retValue;
}

/**
 * @brief Reads the data of a directory.
 *
 * @param name The name of the file to be allocated.
 * @param attrs Attributes of the file.
 * @param sector Pointer for the sector where the file is going to be
 * allocated.
 *
 * @return 0 if the allocation was successful and -1 otherwise.
 */
int directory_rootdir_file_allocate(const char *name, BYTE attrs, DWORD *sector)
{
  if (rootdir_search_empty(name, sector) < 0) {
    return -1;
  }

  Directory *dir = directory_create(name, attrs, 0);
  if (dir == NULL) {
    return -1;
  }

  if (volume_write(*sector, dir, sizeof(*dir)) != 0) {
    free(dir);
    return -1;
  }

  free(dir);
  return 0;
}

/**
 * @brief Reads the data of a directory.
 *
 * @param name The name of the directory to be allocated.
 * @param attrs Attributes of the file.
 * @param sector Pointer for the sector where the directory is going to be
 * allocated.
 *
 * @return 0 if the allocation was successful and -1 otherwise.
 */
static int
  directory_rootdir_dir_allocate(const char *name, BYTE attrs, DWORD *sector)
{
  if (rootdir_search_empty(name, sector) < 0) {
    return -1;
  }

  DWORD cluster = 0;
  if (fat_allocate_chain(1, &cluster) < 0) {
    return -1;
  }

  Directory *dir = directory_create(name, attrs, cluster);
  if (dir == NULL) {
    return -1;
  }

  if (volume_write(*sector, dir, sizeof(*dir)) != 0) {
    free(dir);
    return -1;
  }

  strncpy((char *)dir->DIR_Name, DOT_FILENAME, 11);
  if (volume_write(bpb_cluster_to_sector(cluster), dir, sizeof(*dir)) != 0) {
    free(dir);
    return -1;
  }

  free(dir);
  dir = directory_root_create();
  if (volume_write(
        bpb_cluster_to_sector(cluster) + sizeof(*dir), dir, sizeof(*dir))
      != 0) {
    free(dir);
    return -1;
  }

  free(dir);
  return 0;
}

/**
 * @brief Allocates an file in a subdirectory.
 *
 * @param parser The PathParser structure that stores the path of the file.
 * @param attrs Attributes of the file.
 * @param dirSector The sector of the currently directory.
 * @param sector Pointer for the sector where the file is going to be
 * allocated.
 *
 * @return 0 if the allocation was successful and -1 otherwise.
 */
static int directory_subdir_file_allocate(PathParser *parser,
  BYTE attrs,
  DWORD dirSector,
  DWORD *sector)
{
  int tokenRes = 0;
  int res = -1;
  Directory *dir = NULL;
  Directory *file = NULL;

  do {
    dir = directory_fetch(dirSector);
    if (dir == NULL) {
      break;
    }

    if (strcmp(parser->saveptr, "") == 0) {
      if (subdir_search_empty(
            parser->curToken, directory_cluster(dir), &dirSector)) {
        break;
      }

      file = directory_create(parser->curToken, attrs, 0);
      if (file == NULL) {
        break;
      }

      if (volume_write(dirSector, file, sizeof(*file)) != 0) {
        free(file);
        break;
      }

      res = 0;
      *sector = dirSector;
      break;
    }

    free(dir);
    tokenRes = parser_token(parser);
  } while (tokenRes == 0);

  free(file);
  free(dir);
  return res;
}

/**
 * @brief Reads the data of a directory.
 *
 * @param parser The PathParser structure that stores the path of the directory.
 * @param attrs Attributes of the file.
 * @param dirSector The sector of the currently directory.
 * @param sector Pointer for the sector where the directory is going to be
 * allocated.
 *
 * @return 0 if the allocation was successful and -1 otherwise.
 */
static int directory_subdir_dir_allocate(PathParser *parser,
  BYTE attrs,
  DWORD dirSector,
  DWORD *sector)
{
  int tokenRes = 0;
  int res = -1;
  Directory *dir = NULL;
  Directory *subdir = NULL;

  do {
    dir = directory_fetch(dirSector);
    if (dir == NULL) {
      break;
    }

    if (strcmp(parser->saveptr, "") == 0) {
      if (subdir_search_empty(
            parser->curToken, directory_cluster(dir), &dirSector)) {
        break;
      }

      DWORD cluster = 0;
      if (fat_allocate_chain(1, &cluster) < 0) {
        return -1;
      }

      subdir = directory_create(parser->curToken, attrs, cluster);
      if (subdir == NULL) {
        break;
      }

      if (volume_write(dirSector, subdir, sizeof(*subdir)) != 0) {
        free(subdir);
        break;
      }

      strncpy((char *)subdir->DIR_Name, DOT_FILENAME, 11);
      if (volume_write(bpb_cluster_to_sector(cluster), subdir, sizeof(*dir))
          != 0) {
        free(dir);
        return -1;
      }

      strncpy((char *)dir->DIR_Name, DOTDOT_FILENAME, 11);
      if (volume_write(
            bpb_cluster_to_sector(cluster) + sizeof(*dir), dir, sizeof(*dir))
          != 0) {
        free(dir);
        return -1;
      }

      res = 0;
      *sector = dirSector;
      break;
    }

    free(dir);
    tokenRes = parser_token(parser);
  } while (tokenRes == 0);

  free(subdir);
  free(dir);
  return res;
}

static int directory_update(Directory *dir, DWORD sector, DWORD size)
{
  if (dir == NULL) {
    return -1;
  }

  WORD timestamp = timestamp_current_time();
  WORD datestamp = timestamp_current_date();

  dir->DIR_FileSize = size;
  dir->DIR_WrtDate = datestamp;
  dir->DIR_WrtTime = timestamp;

  if (volume_write(sector, dir, sizeof(*dir)) < 0) {
    return -1;
  }

  return 0;
}

static void directory_save_cluster(Directory *dir, DWORD cluster)
{
  assert("Directory should not be NULL" && dir != NULL);
  dir->DIR_FstClusHI = cluster >> 16;
  dir->DIR_FstClusLO = (WORD)cluster;
}

/******************************************************************************
 *                              PUBLIC FUNCTIONS
 ******************************************************************************/

WORD directory_cluster(const Directory *dir)
{
  assert("Directory should not be NULL" && dir != NULL);
  return ((WORD)(dir->DIR_FstClusHI << 16) | dir->DIR_FstClusLO);
}

Directory *directory_fetch(DWORD sector)
{
  Directory *dir = (Directory *)malloc(sizeof(*dir));
  if (dir == NULL) {
    return NULL;
  }

  if (volume_read(sector, dir, sizeof(*dir)) != 0) {
    return NULL;
  }

  if (dir->DIR_Name[0] == FREE_ENTRY || dir->DIR_Name[0] == LAST_ENTRY) {
    free(dir);
    return NULL;
  }

  return dir;
}

int directory_root_search(const char *path, DWORD *sector)
{
  PathParser *parser = parser_create(path);
  if (parser == NULL || parser->curToken == NULL) {
    return -1;
  }

  int res = -1;
  DWORD dirSector = 0;
  if (rootdir_search(parser->curToken, &dirSector) < 0) {
    goto exit0;
  }

  Directory *dir = directory_fetch(dirSector);
  if (dir == NULL) {
    goto exit0;
  }

  int tokenRes = 0;
  do {
    tokenRes = parser_token(parser);

    if (tokenRes < 0) {
      goto exit1;
    }

    // Last token of the parser. The directory was found.
    if (tokenRes == 1) {
      *sector = dirSector;
      res = 0;
      goto exit1;
    }

    // Make a subdirectory search, with the last directory readed
    if (subdir_search(parser->curToken, directory_cluster(dir), &dirSector)
        < 0) {
      goto exit1;
    }

    free(dir);
    dir = NULL;
    dir = directory_fetch(dirSector);

    if (dir == NULL) {
      goto exit0;
    }
  } while (tokenRes == 0);

exit1:
  free(dir);
exit0:
  parser_delete(&parser);
  return res;
}

int directory_search(const char *path, DWORD curSector, DWORD *sector)
{
  PathParser *parser = parser_create(path);
  if (parser == NULL || parser->curToken == NULL) {
    return -1;
  }

  int res = -1;
  Directory *dir = directory_fetch(curSector);
  if (dir == NULL) {
    goto exit0;
  }

  DWORD dirSector = 0;
  int tokenRes = 0;
  do {
    tokenRes = parser_token(parser);

    if (tokenRes < 0) {
      goto exit1;
    }

    // Last token of the parser. The directory was found.
    if (tokenRes == 1) {
      *sector = dirSector;
      res = 0;
      goto exit1;
    }

    // Make a subdirectory search, with the last directory readed
    if (subdir_search(parser->curToken, directory_cluster(dir), &dirSector)
        < 0) {
      goto exit1;
    }

    free(dir);
    dir = NULL;
    dir = directory_fetch(dirSector);

    if (dir == NULL) {
      goto exit0;
    }
  } while (tokenRes == 0);

exit1:
  free(dir);
exit0:
  parser_delete(&parser);
  return res;
}

ssize_t directory_read(file_t *file, void *buffer, size_t size)
{
  DWORD offset = bpb_cluster_to_sector(file->cluster);
  DWORD clusterSize = bpb_cluster_size();

  // Case 1. The data that needs to be readed is inside the first cluster
  if (size + file->byte <= clusterSize) {
    if (volume_read(offset + file->byte, buffer, size) != 0) {
      return 0;
    }

    file->byte += size;
    return (ssize_t)size;
  }

  // Case 2. The data is in more than one cluster and the byte is different from
  // zero
  if (file->byte != 0 && size + file->byte > clusterSize) {
    DWORD newSize = bpb_cluster_size() - file->byte;
    DWORD newOffset = offset + file->byte;
    if (volume_read(newOffset, buffer, newSize) != 0) {
      return 0;
    }

    if (fat_read(file->cluster, 0, (WORD *)&file->cluster)) {
      return newSize;
    }

    // As we are going to a new cluster the byte offset of it needs to be zero.
    file->byte = 0;
    file->sector = bpb_cluster_to_sector(file->cluster);

    ssize_t res =
      directory_read_data(file, buffer + newSize, size - newSize, file->sector);
    return res + newSize;
  }

  ssize_t res = directory_read_data(file, buffer, size, offset);
  return res;
}

ssize_t directory_write(DWORD file, void *buffer, size_t size)
{
  Directory *dir = directory_fetch(file);
  if (dir == NULL) {
    return -1;
  }

  DWORD clusterSize = bpb_cluster_size();
  DWORD curCluster = 0;
  DWORD numOfFats = (DWORD)CEILING(size, clusterSize);

  if (fat_allocate_chain(numOfFats, &curCluster) < 0) {
    return -1;
  }

  directory_save_cluster(dir, curCluster);

  DWORD offset = bpb_cluster_to_sector(directory_cluster(dir));
  if (size <= clusterSize) {
    if (volume_write(offset, buffer, size) != 0) {
      return 0;
    }

    directory_update(dir, file, (DWORD)size);
    free(dir);
    return (ssize_t)size;
  }

  DWORD nextCluster = 0;
  DWORD numClusters = 0;// Number of clusters being readed
  size_t fileSize = size;
  ssize_t retValue = 0;

  do {
    if (numClusters > numOfFats) {
      free(dir);
      return -1;
    }

    // Read the data as clusters, until we found a "broken" filesize
    if (fileSize > clusterSize) {
      if (volume_write(
            offset, buffer + ((size_t)(clusterSize * numClusters)), clusterSize)
          != 0) {
        retValue = -1;
        break;
      }

      retValue += clusterSize;
      offset += clusterSize;
      fileSize -= clusterSize;
    } else {
      if (volume_write(
            offset, buffer + ((size_t)(clusterSize * numClusters)), fileSize)
          != 0) {
        retValue = -1;
        break;
      }

      retValue += (ssize_t)fileSize;
      offset += fileSize;
    }

    if (fat_read(curCluster, 0, (WORD *)&nextCluster) < 0) {
      break;
    }

    curCluster = nextCluster;
    numClusters++;
  } while (curCluster <= bpb_fat_eof());

  directory_update(dir, file, (DWORD)size);
  free(dir);
  return retValue;
}

int directory_allocate(const char *path, BYTE attrs, DWORD *sector)
{
  PathParser *parser = parser_create(path);
  if (parser == NULL || parser->curToken == NULL) {
    return -1;
  }

  int res = 0;

  DWORD dirSector = 0;
  if (strcmp(parser->saveptr, "") == 0) {
    if (attrs & (BYTE)ATTR_DIRECTORY) {
      res = directory_rootdir_dir_allocate(parser->curToken, attrs, sector);
    } else {
      res = directory_rootdir_file_allocate(parser->curToken, attrs, sector);
    }
  } else {
    // Searches for the entry in the root directory
    if (rootdir_search(parser->curToken, &dirSector) < 0) {
      parser_delete(&parser);
      return -1;
    }

    if (attrs & (BYTE)ATTR_DIRECTORY) {
      res = directory_subdir_dir_allocate(parser, attrs, dirSector, sector);
    } else {
      res = directory_subdir_file_allocate(parser, attrs, dirSector, sector);
    }
  }
  parser_delete(&parser);
  return res;
}

int directory_file_allocate(const char *path, BYTE attrs, DWORD *sector)
{
  PathParser *parser = parser_create(path);
  if (parser == NULL || parser->curToken == NULL) {
    return -1;
  }

  int res = 0;

  DWORD dirSector = 0;
  if (strcmp(parser->saveptr, "") == 0) {
    res = directory_rootdir_file_allocate(parser->curToken, attrs, sector);
  } else {
    // Searches for the entry in the root directory
    if (rootdir_search(parser->curToken, &dirSector) < 0) {
      parser_delete(&parser);
      return -1;
    }

    res = directory_subdir_file_allocate(parser, attrs, dirSector, sector);
  }
  parser_delete(&parser);
  return res;
}

int directory_dir_allocate(const char *path, BYTE attrs, DWORD *sector)
{
  PathParser *parser = parser_create(path);
  if (parser == NULL || parser->curToken == NULL) {
    return -1;
  }

  int res = 0;

  DWORD dirSector = 0;
  if (strcmp(parser->saveptr, "") == 0) {
    res = directory_rootdir_dir_allocate(parser->curToken, attrs, sector);
  } else {
    // Searches for the entry in the root directory
    if (rootdir_search(parser->curToken, &dirSector) < 0) {
      parser_delete(&parser);
      return -1;
    }

    res = directory_subdir_dir_allocate(parser, attrs, dirSector, sector);
  }
  parser_delete(&parser);
  return res;
}

ssize_t directory_list(Directory **dirList, DWORD sector)
{
  Directory *dir = directory_fetch(sector);
  if (dir == NULL) {
    return -1;
  }

  DWORD dirSector = bpb_cluster_to_sector(directory_cluster(dir));
  DWORD offset = dirSector;
  ssize_t numEntries = 0;

  while (1) {
    if (volume_read(offset, dir, sizeof(*dir)) != 0) {
      free(dir);
      return -1;
    }

    if (dir->DIR_Name[0] == LAST_ENTRY) {
      break;
    }

    offset += sizeof(*dir);
    numEntries++;
  }

  *dirList = malloc(sizeof(*dir) * (size_t)numEntries);
  if (*dirList == NULL) {
    free(dir);
    return -1;
  }

  offset = dirSector;
  for (ssize_t i = 0; i < numEntries; offset += sizeof(*dir)) {
    if (volume_read(offset, dir, sizeof(*dir)) != 0) {
      free(dir);
      return -1;
    }

    if (dir->DIR_Name[0] != FREE_ENTRY) {
      memcpy(&(*dirList)[i], dir, sizeof(*dir));
      i++;
    }
  }

  free(dir);
  return numEntries;
}
