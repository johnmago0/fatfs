#include "io/volume.h"
#include <stdio.h>
#include <stdlib.h>

static FILE *volume = NULL;

int volume_open(const char *path)
{
  if (path == NULL) {
    printf("Path is null\n");
    return -1;
  }

  volume = fopen(path, "r+");
  if (volume == NULL) {
    printf("Could not open file: %s\n", path);
    return -1;
  }

  return 0;
}

void volume_close()
{
  fclose(volume);
  volume = NULL;
}

int volume_read(long offset, void *buffer, size_t size)
{
  if (volume == NULL) {
    printf("Volume needs to be initialized\n");
    return -1;
  }

  if (fseek(volume, offset, SEEK_SET) != 0) {
    printf("Could not move during read\n");
    return -1;
  }

  if (fread(buffer, size, 1, volume) != 1) {
    printf("Could not read\n");
    return -1;
  }

  return 0;
}

int volume_write(long offset, void *buffer, size_t size)
{
  if (volume == NULL) {
    printf("Volume needs to be initialized\n");
    return -1;
  }

  if (fseek(volume, offset, SEEK_SET) != 0) {
    printf("Could not move during writing\n");
    return -1;
  }

  if (fwrite(buffer, size, 1, volume) != 1) {
    printf("Could not write\n");
    return -1;
  }

  return 0;
}
