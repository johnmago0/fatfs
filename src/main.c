#include "filesystem/fs_api.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void print_stat(const stat_t *stat)
{
  if (stat == NULL) {
    printf("NULL\n");
    return;
  }

  printf("name: ");
  for (int i = 0; i < 11; i++) {
    printf("%c", (char)stat->name[i]);
  }
  printf("\n");

  printf("attr: %d\n", stat->attr);
  printf("createTime: %d\n", stat->createTime);
  printf("writeTime: %d\n", stat->writeTime);
  printf("createDate: %d\n", stat->createDate);
  printf("writeDate: %d\n", stat->writeDate);
  printf("lastAccDate: %d\n", stat->lastAccDate);
  printf("cluster: %d\n", stat->cluster);
  printf("fileSize: %d\n", stat->fileSize);
  printf("\n");
}

int main(int argc, char *argv[])
{
  if (argc < 2) {
    printf("Incorrect number of arguments.\n");
    printf("Usaged: %s <filename>\n", argv[0]);
    return 1;
  }

  if (fs_mount(argv[1]) < 0) {
    return -1;
  }

  file_t *file = fs_open(argv[2]);
  if (file == NULL) {
    printf("NULL\n");
    return -1;
  }

  for (int i = 0; i < 3; i++) {
    size_t size = 0;
    if (i == 0) {
      size = sizeof(char) * 1000;
    } else if (i == 1) {
      size = sizeof(char) * 2048;
    } else {
      size = sizeof(char) * 3000;
    }

    void *buffer = calloc(1, size);
    if (buffer == NULL) {
      return -1;
    }

    ssize_t resSize = fs_read(file, buffer, size);
    if (resSize < 0) {
      printf("Couldn't read\n");
      free(buffer);
      return -1;
    } else {
      for (size_t i = 0; i < size; i++) {
        printf("%c", ((char *)buffer)[i]);
      }
      printf("\n");
    }

    free(buffer);
  }

  fs_close(file);
  fs_umount();
  return 0;
}
