#ifndef PATH_PARSER_H
#define PATH_PARSER_H

#include <stdbool.h>

#define DOT_FILENAME ".          "
#define DOTDOT_FILENAME "..         "

typedef struct {
  char *saveptr; // To be used with strok_r
  char *path;
  char *curToken;
} PathParser;

/**
 * @brief Creates a new PathParser
 *
 * @param path Filepath to be parser. It must be a null-terminated string.
 *
 * @return a malloc allocated PathParser if successfuly analysed, and NULL
 * otherwise.
 */
PathParser *parser_create(const char *path);

/**
 * @brief Creates a new PathParser
 *
 * @param parser PathParser pointer to be deallocated.
 */
void parser_delete(PathParser **parser);

/**
 * @brief Get the next token of the PathParser
 *
 * This function utilizes the path saved by the filepath_open. It splits this
 * filepath into smaller tokens to be used in search
 *
 * @param parser PathParser pointer to be used during parser.
 *
 * @return 0 if the parser was successful, 1 if reached end of filepath and -1
 * otherwise
 */
int parser_token(PathParser *parser);

#endif // PATH_PARSER_H
