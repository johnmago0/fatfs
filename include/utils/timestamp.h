#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include "../filesystem/type.h"

// TODO: This may cause problems in the case of different localizations

#define DAY(date) ((date) & 0x1F)
#define MONTH(date) (((date) >> 5) & 0x0F)
#define YEAR(date) ((((date) >> 9) & 0xFF) + 1980)
#define DATE_STAMP(day, month, year)                                           \
  ((day) | ((month) << 5) | (((year) - 1980) << 9))

#define HOURS(time) (((time) >> 11) & 0xFF)
#define MINUTES(time) (((time) >> 5) & 0x3F)
#define SECONDS(time) (((time) & 0x1F) * (BYTE)2)
#define TIME_STAMP(hours, minutes, seconds)                                    \
  (((hours) << 11) | ((minutes) << 5) | ((seconds) / 2))

/**
 * @brief Gets the current time as a timestamp.
 */
WORD timestamp_current_time();

/**
 * @brief Gets the current date as a timestamp.
 */
WORD timestamp_current_date();

/**
 * @brief Gets the current time on milliseconds.
 */
BYTE timestamp_current_milliseconds();

#endif // TIMESTAMP_H
