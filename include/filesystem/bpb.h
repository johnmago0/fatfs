#ifndef BPB_H
#define BPB_H

#include "type.h"

#define FAT12 12
#define FAT16 16
#define FAT32 32

// FAT12/16
typedef struct __attribute__((packed))
{
  BYTE BS_DrvNum;
  BYTE BS_Reserved1;
  BYTE BS_BootSig;
  DWORD BS_VolID;
  BYTE BS_VolLab[11];
  BYTE BS_FilSysType[8];
} BPB16;

// FAT32
typedef struct __attribute__((packed))
{
  DWORD BPB_FATSz32;
  WORD BPB_ExtFlags;
  WORD BPB_FSVer;
  DWORD BPB_RootClus;
  WORD BPB_FSInfo;
  WORD BPB_BKBootSec;
  BYTE BPB_Reserved[12];
  BYTE BS_DrvNum;
  BYTE BS_Reserved1;
} BPB32;

typedef union __attribute__((packed)) {
  BPB16 block16;
  BPB32 block32;
} BlockExt;

// Common
typedef struct __attribute__((packed))
{
  BYTE BS_jmpBoot[3];
  BYTE BS_OEMName[8];
  WORD BPB_BytsPerSec;
  BYTE BPB_SecPerClus;
  WORD BPB_RsvdSecCnt;
  BYTE BPB_NumFATs;
  WORD BPB_RootEntCnt;
  WORD BPB_TotSec16;
  BYTE BPB_Media;
  WORD BPB_FATSz16;
  WORD BPB_SecPerTrk;
  WORD BPB_NumHeads;
  DWORD BPB_HiddSec;
  DWORD BPB_TotSec32;
  BlockExt extension;
} BPB;

/**
 * @brief Open the BPB structure of the filesystem.
 *
 * @return 0 if the open was successful and -1 otherwise
 */
int bpb_open();

/**
 * @brief Closes the BPB structure.
 */
void bpb_close();

/**
 * @brief Returns the size of a FAT.
 */
DWORD bpb_fat_size();

/**
 * @brief Returns the EOF of the currently FAT.
 */
DWORD bpb_fat_eof();

/**
 * @brief Returns the FAT offset of the specified cluster.
 *
 * The FAT structure start in 1.
 */
DWORD bpb_fat_offset(DWORD cluster);

/**
 * @brief Returns the FAT entry of the offset;
 */
DWORD bpb_fat_entry_offset(DWORD offset);

/**
 * @brief Returns the sector of this FAT offset.
 */
DWORD bpb_fat_sec_num(DWORD offset);

/**
 * @brief Returns count of clusters in the FAT volume.
 */
DWORD bpb_count_of_clusters();

/**
 * @brief Return the total entries in the root directory.
 */
DWORD bpb_root_entry_count();

/**
 * @brief Returns the sector of the root directory.
 */
DWORD bpb_root_sector();

/**
 * @brief Returns the first sector of the cluster(in bytes).
 */
DWORD bpb_cluster_to_sector(DWORD cluster);

/**
 * @brief Returns the size of a cluster(in bytes).
 */
DWORD bpb_cluster_size();

/**
 * @brief Returns the number of bytes per sector.
 */
DWORD bpb_bytes_per_sec();

#endif// BPB_H
