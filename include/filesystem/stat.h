#ifndef STAT_TYPE_H
#define STAT_TYPE_H

#include "type.h"

typedef struct
{
  DWORD cluster;
  DWORD fileSize;
  WORD createTime;
  WORD writeTime;
  WORD createDate;
  WORD writeDate;
  WORD lastAccDate;
  char name[11];
  BYTE attr;
} stat_t;

#endif// STAT_TYPE_H
