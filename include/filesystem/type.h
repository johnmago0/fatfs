#ifndef TYPE_H
#define TYPE_H

#include <stdint.h>

#define CEILING(x, y) (((x) + ((y) - 1)) / (y))

#define BYTE uint8_t
#define WORD uint16_t
#define DWORD uint32_t

#endif// TYPE_H
