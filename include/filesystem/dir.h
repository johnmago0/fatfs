#ifndef DIR_TYPE_H
#define DIR_TYPE_H

#include "stat.h"
#include "type.h"

typedef struct
{
  DWORD curSector;
  DWORD dotdotSector;
  stat_t *stat;
  BYTE isRoot;
} dir_t;


#endif// DIR_TYPE_H
