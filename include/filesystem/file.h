#ifndef FILE_TYPE_H
#define FILE_TYPE_H

#include "stat.h"
#include "type.h"

typedef struct
{
  DWORD cluster;
  DWORD sector;
  DWORD byte;
  stat_t *stat;
} file_t;

#endif// FILE_TYPE_H
