#ifndef FILESYSTEM_FAT_H
#define FILESYSTEM_FAT_H

#include "type.h"

/**
 * @brief Read a cluster in the FAT structure
 *
 * @param cluster The current cluster that needs to be readed
 * @param fat Number of the FAT that is going to be readed. The value of this
 * should be 0 by default.
 * @param dest Destination address where the cluster is going to be readed
 *
 * @return 0 if the cluster could be readed and -1 otherwise
 */
int fat_read(DWORD cluster, BYTE fat, WORD *dest);

/**
 * @brief Write a cluster in the FAT structure
 *
 * @param cluster The current cluster that is going to be writted
 * @param entryVal Value set in the cluster specified
 *
 * @return 0 if the cluster could be writed and -1 otherwise
 */
int fat_write(DWORD cluster, WORD entryVal);

/**
 * @brief Allocate a chain of clusters in FAT structure
 *
 * @param nclusters The number of FATs that need to be allocated
 * @param cluster The first cluster of the chain of FATs
 *
 * @return 0 if the cluster chain could be allocated and -1 otherwise
 */
int fat_allocate_chain(DWORD nclusters, DWORD *cluster);

/**
 * @brief Deallocate a chain of clusters in FAT structure
 *
 * @param cluster The first cluster of the chain of FATs
 *
 * @return 0 if the cluster chain could be deallocated and -1 otherwise
 */
int fat_deallocate_chain(DWORD cluster);

/**
 * @brief Allocate a new cluster in then end of the FAT chain
 *
 * @param cluster A cluster that is part of the chain, to start the search
 * @param newCluster The new allocated cluster
 *
 * @return 0 if the new cluster could be allocated and -1 otherwise
 */
int fat_allocate_cluster(DWORD cluster, DWORD *newCluster);

/**
 * @brief Deallocate a cluster in the end of the FAT chain
 *
 * @param cluster A cluster that is part of the chain, to start the search.
 * This cluster should not be the last on the chain (use fat_deallocate_chain
 * instead).
 *
 * @return 0 if the cluster chain could be deallocated and -1 otherwise
 */
int fat_deallocate_cluster(DWORD cluster);

#endif// FILESYSTEM_FAT_H
