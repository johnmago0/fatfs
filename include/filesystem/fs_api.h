#ifndef FILESYSTEM_API_H
#define FILESYSTEM_API_H

#include "dir.h"
#include "file.h"
#include "stat.h"
#include "type.h"

#include <stdlib.h>

#define READ_ONLY 0x01
#define HIDDEN 0x02

typedef enum {
  SET,
  CUR,
  END,
} seek_t;

/**
 * @brief Mounts the filesystem.
 *
 * @param volume The path for the volume to be opened.
 *
 * @return 0 if the mount was successful and -1 otherwise
 */
int fs_mount(const char *volume);

/**
 * @brief Umount filesystem.
 */
void fs_umount();

/**
 * @brief Opens a file in the filesystem.
 *
 * @param path Path for the file to be open
 *
 * @return New allocated file if the open was successful and NULL otherwise
 */
file_t *fs_open(const char *path);

/**
 * @brief Opens a directory in the filesystem.
 *
 * @param path Path for the directory to be open
 *
 * @return New allocated directory if the open was successful and NULL
 * otherwise
 */
dir_t *fs_opendir(const char *path);

/**
 * @brief Closes a open directory.
 *
 * @param dir The allocated dir that need to be deallocated
 */
void fs_closedir(dir_t *dir);

/**
 * @brief Closes a open file.
 *
 * @param file The allocated file that need to be deallocated
 */
void fs_close(file_t *file);

/**
 * @brief Returns the statuts of a file.
 *
 * @param file The allocated file that is going to be readed
 *
 * @return New allocated stat_t structure if successful readed, and NULL
 * otherwise.
 */
stat_t *fs_stat(file_t *file);

/**
 * @brief Returns the statuts of a directory.
 *
 * @param dir The allocated directory that is going to be readed
 *
 * @return New allocated stat_t structure if successful readed, and NULL
 * otherwise.
 */
stat_t *fs_statdir(dir_t *dir);

/**
 * @brief Place the file pointer into the offset passed. This seeking will
 * happen based on the start of the file location
 *
 * @param file Pointer for the file that is going to be writted
 * @param offset The offset were the pointer of the file is going to be placed
 * @param seek The type of the seek that can is going to be used. The valid
 * possibilities are the following:
 *      SET: Set the file pointer starting from the first address of the file.
 *      CUR: Set the file pointer based on the current address on the file.
 *      END: Set the file pointer starting from the end address of the file.
 *
 * @return 0 if the offset could be seeked, or -1 otherwise.
 */
int fs_seek(file_t *file, long offset, seek_t seek);

/**
 * @brief Reads the content of a file into a buffer.
 *
 * @param file The allocated file that need to be readed
 * @param buffer The buffer were the data is going to be readed
 * @param size The total number of bytes in the buffer
 *
 * @return The total size of bytes readed. If something went wrong a negative
 * number will be returned instead
 */
ssize_t fs_read(file_t *file, void *buffer, size_t size);

/**
 * @brief Writes the content of a buffer into a file.
 *
 * @param file Pointer for the file that is going to be writted
 * @param buffer The buffer of the data
 * @param size The total number of bytes in the buffer
 *
 * @return The total size of bytes writted. If something went wrong a negative
 * number will be returned instead
 */
ssize_t fs_write(file_t *file, void *buffer, size_t size);

/**
 * @brief Allocates a file on the filesystem.
 *
 * @param path The allocated file that need to be deallocated
 * @param attr Attributes for the file to be created
 *
 * @return New allocated file_t if successfuly created and NULL otherwise
 */
file_t *fs_mkfile(const char *path, BYTE attr);

/**
 * @brief Allocates a directory on the filesystem.
 *
 * @param path The allocated directory that need to be deallocated
 * @param attr Attributes for the directory to be created
 *
 * @return New allocated directory if successfuly created and NULL otherwise
 */
dir_t *fs_mkdir(const char *path, BYTE attr);

#endif// FILESYSTEM_API_H
