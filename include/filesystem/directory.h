#ifndef DIRECTORY_H
#define DIRECTORY_H

#include "file.h"
#include "type.h"

#include <stdlib.h>

#define FREE_ENTRY 0xE5
#define LAST_ENTRY 0x00

#define ATTR_READ_ONLY 0x01
#define ATTR_HIDDEN 0x02
#define ATTR_SYSTEM 0x04
#define ATTR_VOLUME_ID 0x08
#define ATTR_DIRECTORY 0x10
#define ATTR_ARCHIVE 0x20
#define ATTR_LONG_NAME \
  (ATTR_READ_ONLY | ATTR_HIDDEN | ATTR_SYSTEM | ATTR_VOLUME_ID)

typedef struct __attribute__((packed))
{
  BYTE DIR_Name[11];
  BYTE DIR_Attr;
  BYTE DIR_NTRes;
  BYTE DIR_CrtTimeTenth;
  WORD DIR_CrtTime;
  WORD DIR_CrtDate;
  WORD DIR_LstAccDate;
  WORD DIR_FstClusHI;
  WORD DIR_WrtTime;
  WORD DIR_WrtDate;
  WORD DIR_FstClusLO;
  DWORD DIR_FileSize;
} Directory;

/**
 * @brief Searches for a file in the root directory
 *
 * @param path The filepath to be searched
 * @param sector The pointer were the sector is going to be saved
 *
 * @return 0 if the file could be found, and -1 otherwise.
 */
int directory_root_search(const char *path, DWORD *sector);

/**
 * @brief Searches for a file in the root directory
 *
 * @param path The filepath to be searched
 * @param curSector The sector to start the searched
 * @param sector The pointer were the sector is going to be saved
 *
 * @return 0 if the file could be found, and -1 otherwise.
 */
int directory_search(const char *path, DWORD curSector, DWORD *sector);

/**
 * @brief Read a cluster of a Directory structure
 *
 * @param dir Pointer for the Directory structure
 *
 * @return The cluster number of the directory
 */
WORD directory_cluster(const Directory *dir);

/**
 * @brief Read a Directory structure
 *
 * @param sector Offset of the directory
 *
 * @return A new allocated Directory structure if successful readed, and NULL
 * otherwise.
 */
Directory *directory_fetch(DWORD sector);

/**
 * @brief Read the data of a directory
 *
 * @param file The pointer of the file
 * @param buffer The buffer where the data will be readed
 * @param size The number of bytes that need to be readed from the file
 *
 * @return The total size of bytes readed, or a negative number if something
 * went wrong
 */
ssize_t directory_read(file_t *file, void *buffer, size_t size);

/**
 * @brief Write data on a directory
 *
 * @param file The sector where the directory can be found
 * @param buffer The buffer that stores the data to write
 * @param size The number of bytes that need to be written from the file
 *
 * @return The total size of bytes written, or a negative number if something
 * went wrong
 */
ssize_t directory_write(DWORD file, void *buffer, size_t size);

/**
 * @brief Allocates a node into the directory structure
 *
 * @param path Path where the directory needs to be allocated
 * @param attrs Attributes that the file will have
 * @param sector Pointer for the sector where this directory can be found
 *
 * @return 0 if the allocation was successful and -1 otherwise
 */
int directory_allocate(const char *path, BYTE attrs, DWORD *sector);

/**
 * @brief Allocates a file into the directory structure
 *
 * @param path Path where the directory needs to be allocated
 * @param attrs Attributes that the file will have
 * @param sector Pointer for the sector where this directory can be found
 *
 * @return 0 if the allocation was successful and -1 otherwise
 */
int directory_file_allocate(const char *path, BYTE attrs, DWORD *sector);

/**
 * @brief Allocates a directory into the directory structure
 *
 * @param path Path where the directory needs to be allocated
 * @param attrs Attributes that the file will have
 * @param sector Pointer for the sector where this directory can be found
 *
 * @return 0 if the allocation was successful and -1 otherwise
 */
int directory_dir_allocate(const char *path, BYTE attrs, DWORD *sector);

#endif// DIRECTORY_H
