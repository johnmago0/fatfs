#ifndef VOLUME_H
#define VOLUME_H

#include <stdlib.h>

/**
 * @brief Opens a volume file stream, specified by a path.
 *
 * @param path The path for the volume to be opened.
 *
 * @return 0 if the open was successful and -1 otherwise
 */
int volume_open(const char *path);

/**
 * @brief Close the volume.
 */
void volume_close();

/**
 * @brief Reads data from the volume file stream at a specified offset into a
 * buffer.
 *
 * @param offset The offset from the beginning of the file to start reading.
 * @param buffer A pointer to the buffer where the read data will be stored.
 * @param size The size of the data to be read, in bytes.
 *
 * @return 0 if the read was successful and -1 otherwise
 */
int volume_read(long int offset, void *buffer, size_t size);

/**
 * @brief Writes data from the volume file stream at a specified offset into a
 * buffer.
 *
 * @param offset The offset from the beginning of the file to start reading.
 * @param buffer A pointer to the buffer where the read data will be stored.
 * @param size The size of the data to be read, in bytes.
 *
 * @return 0 if the write was successful and -1 otherwise
 */
int volume_write(long int offset, void *buffer, size_t size);

#endif // VOLUME_H
